import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class TwosComplementTest {
    @Test
    void playground() {
        TwosComplement twosComplement = new TwosComplement(false, true, false, true, false);
        twosComplement.equals(new TwosComplement(10));
        twosComplement.invertBits();
    }

    @org.junit.jupiter.api.Test
    void invertBits() {
        TwosComplement test = new TwosComplement(true, false, true, false);
        test.invertBits();
        assertTrue(test.equals(new TwosComplement(false, true, false, true)));
    }


    @org.junit.jupiter.api.Test
    void convertIntegerToTwosComplement() {
        TwosComplement test = new TwosComplement((short)4);
        test.convertIntegerToTwosComplement(-5);
        assertTrue(test.equals(new TwosComplement(true, false, true, true)));
    }


    @Test
    void add() {
        TwosComplement a = new TwosComplement((short)4);
        TwosComplement b = new TwosComplement((short)4);


        a.add(b);

        assertTrue((a.getBitAt(0)==false) && (a.getBitAt(1)==false)  && (a.getBitAt(2)==false)  && (a.getBitAt(3)==false));

        TwosComplement c = new TwosComplement(true, false, true, false);
        TwosComplement d = new TwosComplement(false, false, true, true);

        c.add(d);
        assertTrue((c.getBitAt(0)==true) && (c.getBitAt(1)==true)  && (c.getBitAt(2)==false)  && (c.getBitAt(3)==true));



    }


    @Test
    void equals() {
        TwosComplement a = new TwosComplement(true, true, true, true, false, false);
        TwosComplement b = new TwosComplement(true, true, false, false);
        assertEquals(a.equals(b), b.equals(a));
    }



    @Test
    void addInt() {
        TwosComplement b = new TwosComplement(5);
        b.add(-8);
        assertTrue(b.equals(new TwosComplement(true, false, true)));

    }

    @Test
    void constructors() {
        TwosComplement a = new TwosComplement(8);
        assertEquals(a.bitSize, 5);
        a = new TwosComplement(-32);
        assertEquals(a.bitSize, 6);
        assertTrue(a.equals(new TwosComplement(true, false, false, false, false, false)));
    }
}