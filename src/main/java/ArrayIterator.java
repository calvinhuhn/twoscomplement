import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayIterator implements Iterator<Boolean> {

    private boolean array[];
    private int pos = 0;

    public ArrayIterator(boolean anArray[])
    {
        array = anArray;
        pos = anArray.length - 1;

    }

    public boolean hasNext()
    {
        return pos >= 0;
    }

    public Boolean next() throws NoSuchElementException
    {
        if ( hasNext() )
            return array[pos--];
        else
            throw new NoSuchElementException();
    }

    public void remove()
    {
        throw new UnsupportedOperationException();
    }
}
