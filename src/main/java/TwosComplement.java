import java.util.Iterator;

public class TwosComplement implements Iterable<TwosComplement>{
    boolean[] bits;
    short bitSize;

    public TwosComplement(short bitSize) {
        bits = new boolean[bitSize];
        this.bitSize = bitSize;
        initBits();
    }

    public TwosComplement(boolean... parameters) {

        this.bits = parameters;
        bitSize = (short)bits.length;
    }

    public  TwosComplement(int number){
        this((short)2);
        convertIntegerToTwosComplement(number);

    }





    public Iterator iterator() {
        return new ArrayIterator(bits);
    }

    @Override
    public String toString() {
        Iterator iterator = this.iterator();
        String ausgabe = "";
        while (iterator.hasNext()){
            ausgabe = "" + (((Boolean)iterator.next() == true)? 1 : 0) + ausgabe;
        }
        return ausgabe;
    }

    /**
     * Replaces the bit at the specified position in this list with the specified bit
     * @param index index of the element to replace
     * @param binary bit to be stored at the specified position
     */
    private void changeBitAt(int index, boolean binary){
        try {
            bits[index] = binary;
        } catch (IndexOutOfBoundsException e){
            //got into an overflow
        }
    }

    /**
     * Inverts all Bits in the bit Array
     */
    public void invertBits() {
        for (int i = 0; i < bitSize; i++){
            bits[i] = !bits[i];
        }
    }

    /**
     * Returns the bit at the specified position.
     * @param index index of the element to return
     * @return the bit at the specified position. If the position is not valid, it returns fales, but does not throw an error.
     */
    public boolean getBitAt(int index) {
       try {
           return bits[index];
       } catch (IndexOutOfBoundsException e){
           return false;
       }


    }


    /**
     * Converts an positive Integer to the binary Array.
     * @param decimal descibes the to be converted Integer
     */
    private void convertPositiveDecimalToBinary(int decimal) {

        initBits();

        int remain, positionalNotation = bits.length - 1;
        int integerquotient = decimal;
        do {
            remain = integerquotient % 2;
            this.changeBitAt(positionalNotation, (remain == 1));
            positionalNotation--;
            integerquotient = integerquotient / 2;
        } while (integerquotient!=0);
    }


    private void initBits() {

        for (int i = 0; i < bits.length; i++){
            bits[i] = false;
        }
    }

    private void adNeededBits(int a) {
        if (a > 0){
            changeBitsize((short)(Math.ceil(Math.log(a + 1) / Math.log(2)) + 1));


        } else if (a < 0){
            changeBitsize((short)((Math.ceil(Math.log( a * -1)) / Math.log(2)) + 1));
        }


    }
    public void convertIntegerToTwosComplement(int a) {
        if (!numberInValidRange(a)) {
              this.adNeededBits(a);
            //throw new NumberOutOfRangeException("test");
        }
        convertPositiveDecimalToBinary(Math.abs(a));

        if(a<0) {
            this.invertBits();

            TwosComplement eins = new TwosComplement(bitSize);
            eins.changeBitAt(bitSize - 1, true);
            this.add(eins);
        }

    }

    private boolean bitLengthEquals(TwosComplement o) {
        return this.bitSize == o.bitSize;
    }


    private void changeBitsize(short newBitSize) {
        boolean[] newBits = new boolean[newBitSize];
        boolean msb = bits[0];
        for (int i = 0; i < newBitSize; i++) {
            if (i < newBits.length - bitSize){
                newBits[i] = msb;
            } else {
                newBits[i] = this.bits[i - (newBits.length - bitSize)];
            }

        }
        bits = newBits;
        bitSize = (short)bits.length;

    }

    /**
     * Compares the specified object with this Binary for equality.
     * Returns true if and only if the specified objects corresponding Integer equals the others corresponding Integer.
     * This pair of elements do not have to be the same size.
     * Eg. 1111101 equals 101 or 0000000101 equals 0101.
     * 0101 does not equal 101, because the first part is the negative/positive sign
     * @param o
     * @return
     */
    public boolean equals(TwosComplement o){
        if (!this.bitLengthEquals(o)) {
            if (this.bitSize > o.bitSize){
                o.changeBitsize(this.bitSize);
            } else {
                this.changeBitsize(o.bitSize);
            }
        }
        for (int i = 0; i < bitSize; i++) {
            if (this.getBitAt(i)!=o.getBitAt(i)) return false;
        }
        return true;
    }


    /**
     * Checks, if the number is within the valid range determined by the bitsize
     * @param a the number to be checked
     * @return true is it is in valid range, false if not
     */
    private boolean numberInValidRange(int a){
        return a < Math.pow(2.0, (bitSize - 1))  && a >= -1 * Math.pow(2, (bitSize - 1));
    }

    /**
     * Adds an integer to the Twos Complement
     * @param decimal the integer to be added
     */
    public void add(int decimal) {
        TwosComplement a = new TwosComplement(decimal);
        this.add(a);
    }


    /** Appends the specified binary element to the this twos complement.
     * This Twos Complement and the one to be added do not have to be the same size. They smaller one is getting extended.
     * @param bitsToBeAdded twos complement element to be added
     */
    public void add(TwosComplement bitsToBeAdded){
        //makes sure, we have the same bitsize. Otherwise extend the bits.
        if (!this.bitLengthEquals(bitsToBeAdded)){
            if (this.bitSize > bitsToBeAdded.bitSize){
                bitsToBeAdded.changeBitsize(this.bitSize);
            } else {
                this.changeBitsize(bitsToBeAdded.bitSize);
            }
        }
        TwosComplement carryBits = new TwosComplement(bitSize);
        //addition
        for (int i = bitSize-1; i >= 0; i--){
            if (carryBits.getBitAt(i) && bitsToBeAdded.getBitAt(i) && this.getBitAt(i)) {
                //3 times 1
                this.changeBitAt(i, true);
                carryBits.changeBitAt(i - 1, true);
            } else if (!carryBits.getBitAt(i) && !bitsToBeAdded.getBitAt(i) && !this.getBitAt(i)) {
                //0 times true
                changeBitAt(i, false); //Eigentlich unnötig
            } else if ((carryBits.getBitAt(i) && bitsToBeAdded.getBitAt(i)) || (carryBits.getBitAt(i) && this.getBitAt(i)) || (this.getBitAt(i) && bitsToBeAdded.getBitAt(i))){
                //two true
                this.changeBitAt(i, false);
                carryBits.changeBitAt(i - 1, true);
            } else {
                //one true
                this.changeBitAt(i, true);
            }
        }
    }
}
