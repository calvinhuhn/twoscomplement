import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Zweierkomplement {
    public static final short bitSize = 4;




    public static void main(String[] args) {
        TwosComplement binaryNumber;
        String eingabe = "";
        while (eingabe != "exit") {
            try {
                binaryNumber = new TwosComplement(bitSize);
                System.out.print("Term eingeben: ");
                InputStreamReader streamReader = new InputStreamReader(System.in);
                BufferedReader bufferedReader = new BufferedReader(streamReader);
                eingabe = bufferedReader.readLine();
                String interpreter = eingabe.replace("--", "+").replace("+", " +").replace("-", " -");
                try {
                    List<Integer> elements = Arrays.stream(interpreter.split(" "))
                            .filter(item-> !item.isEmpty() && !item.isBlank())
                            .map(Integer::valueOf)
                            .collect(Collectors.toList());

                    binaryNumber.convertIntegerToTwosComplement(elements.get(0));
                    elements.remove(0);
                    for (Integer numbers:
                         elements) {
                        binaryNumber.add(numbers);

                    }
                    System.out.println(eingabe + " -> " + binaryNumber);

                } catch (NumberFormatException e) {
                    System.out.println("Ungültige eingabe. Beispieleingaben: exit, -6+3 39845");
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
        }




    }
}
